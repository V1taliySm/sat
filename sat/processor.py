# -*- coding: utf-8 -*-

from django.conf import settings
from page.models import Page


def base_data(request):
    data = {
        "pages": Page.objects.all().order_by('order'),
    }
    return data
