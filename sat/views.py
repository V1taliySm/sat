from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.utils.translation import activate, get_language, LANGUAGE_SESSION_KEY
from page.models import Page
from collection.models import Collection
from pure_pagination.paginator import Paginator

def index(request, template_name="index.html"):
    data = {}
    collections = Collection.objects.all()
    try:
        page_no = int(request.GET.get('page', 1))
    except (TypeError, ValueError):
        raise Http404()
    if page_no < 1:
        raise Http404()
    paginator = Paginator(collections, 1, request=request)
    try:
        collection = paginator.page(page_no)
    except:
        raise Http404()
    data['collection'] = collection
    data['paginator'] = paginator
    return render(request, template_name, data)


def lang(request, lang=None):
    if not lang:
        lang = 'uk'
    activate(lang)
    request.session[LANGUAGE_SESSION_KEY] = lang
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))