# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from ckeditor.fields import RichTextField


class Page(models.Model): 
    """
    Page
    """
    title = models.CharField(verbose_name=_("Назва"), max_length=250, db_index=True, blank=True, null=True)
    description =  RichTextField(verbose_name=_("Опис сторінки"), null=True, blank=True)
    order = models.PositiveIntegerField(default=0, verbose_name=_("Порядок відображення"))

    def __unicode__(self):
        return u"%s" % (self.title)


    class Meta:
        verbose_name = _("Додаткова інформаційна сторінка")
        verbose_name_plural = _("Додаткові інформаційні сторінки")