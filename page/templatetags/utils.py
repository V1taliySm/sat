

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
def splitq(queryset, n):
    print(len(queryset))
    n = n.split(':')
    part_length = int(n[0])
    number = int(n[1])
    return [queryset[:part_length], queryset[part_length:]][number]


