from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Page

def page(request, pk=None, template_name="page.html"):
    page = get_object_or_404(Page, pk=pk)
    return render(request, template_name, {"page": page})