# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Page

class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'order')    
    search_fields = ('title', 'description',)


admin.site.register(Page, PageAdmin)