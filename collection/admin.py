# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Collection

class CollectionAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'published')    
    search_fields = ('title', 'description', 'author')
    list_filter = ('author',)


admin.site.register(Collection, CollectionAdmin)