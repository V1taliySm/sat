from django.urls import path
from .views import collection

urlpatterns = [
    path('<int:pk>/', collection, name="collection"),
]