from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Collection

def collection(request, pk=None, template_name="collection.html"):
    collection = get_object_or_404(Collection, pk=pk)
    return render(request, template_name, {"collection": collection})