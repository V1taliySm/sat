from django.conf import settings
def resize_image_width(image_width, image_height):
    width_percent = (settings.DEFAULT_IMAGE_WIDTH/float(image_width))
    print (width_percent)
    new_image_height = int((float(image_height) * float(width_percent)))

    return settings.DEFAULT_IMAGE_WIDTH, new_image_height


def resize_image_height(image_width, image_height):
    height_percent = (settings.DEFAULT_IMAGE_HEIGHT/float(image_height))
    print (height_percent)
    new_image_width = int((float(image_width) * float(height_percent)))
    return new_image_width, settings.DEFAULT_IMAGE_HEIGHT