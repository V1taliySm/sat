# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from ckeditor.fields import RichTextField
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys
from .utils import resize_image_width, resize_image_height

class Collection(models.Model): 
    """
    Collection
    """
    title = models.CharField(verbose_name=_("Назва"), max_length=250, db_index=True, blank=True, null=True)
    sub_title = models.CharField(verbose_name=_("Підзаголовок"), max_length=250, db_index=True, blank=True, null=True)
    author = models.CharField(verbose_name=_("Автор"), max_length=250, db_index=True, blank=True, null=True)
    sub_author = models.CharField(verbose_name=_("Помічники автора"), max_length=250, db_index=True, blank=True, null=True)
    published = models.DateTimeField(verbose_name=_("Дата публікації"), null=True, blank=True, db_index=True)
    image = models.ImageField(upload_to='images/%Y/%m/%d/', verbose_name=_("Зображення"), max_length=250, blank=True, null=True)
    document = models.FileField(upload_to='domuments/%Y/%m/%d/', verbose_name=_("Документ/Файл"), max_length=250, blank=True, null=True)
    description =  RichTextField(verbose_name=_("Опис збірника"), null=True, blank=True)

    def __unicode__(self):
        return u"%s" % (self.title)


    def save(self, *args, **kwargs):
        if self.image:
            imageTemproary = Image.open(self.image)
            image_width, image_height = imageTemproary.size
            print (image_width, image_height)
            if image_width > settings.DEFAULT_IMAGE_WIDTH:
                image_width, image_height  = resize_image_width(image_width, image_height)
            print (image_width, image_height)
            if image_height > settings.DEFAULT_IMAGE_HEIGHT:
                image_width, image_height  = resize_image_height(image_width, image_height)
            print (image_width, image_height)
            imageTemproary = imageTemproary.resize((image_width, image_height), Image.ANTIALIAS)
            background = Image.new('RGB', (settings.BACKGROUND_WIDTH, settings.BACKGROUND_HEIGHT), color = "white")
            offset = ((settings.BACKGROUND_WIDTH - image_width) // 2, (settings.BACKGROUND_HEIGHT - image_height) // 2)
            background.paste(imageTemproary, offset)
            outputIoStream = BytesIO()
            background.save(outputIoStream , format='PNG')
            outputIoStream.seek(0)
            self.image = InMemoryUploadedFile(outputIoStream,'ImageField', "%s.png" %self.image.name.split('.')[0], 'image/png', sys.getsizeof(outputIoStream), None)
        super(Collection, self).save(*args, **kwargs)


    class Meta:
        verbose_name = _("Збірник")
        verbose_name_plural = _("Збірники")