from modeltranslation.translator import translator, TranslationOptions
from .models import Collection

class CollectionTranslationOptions(TranslationOptions):
    fields = ('title', 'sub_title', 'author', 'sub_author', 'description')

translator.register(Collection, CollectionTranslationOptions)